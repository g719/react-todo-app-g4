import React, { useState } from 'react'
import EdiText from 'react-editext'

function Todo({ todo, toggleTodo, todos, setTodos }) {
    const [todoEditing, setTodoEditing] = useState(null)                               // Id of the todo we editing
    const [editingText, setEditingText] = useState("")


    // date, when the todo was created, YYYY-MM-DD.
    const date = new Date(todo.id).toLocaleDateString();

    // const [value, setValue] = useState();

    const deleteHandler = () => {
        setTodos(todos.filter((el) => el.id !== todo.id));
    };

    const todoChecked = () => {
        toggleTodo(todo.id, todo);
    };

    //const handleSave = (val) => {
    //    setValue(val);
    //};

    function editTodo(id) {                               // takes a todo's id
        const updatedTodos = [...todos].map((todo) => {
            if (todo.id === id) {                              // if todo id is = selected todo id 
                todo.name = editingText
            }
            return todo                                      // return todo with applied changes made
        })
        setTodos(updatedTodos)                              // Updated todo with new changes applied
        setTodoEditing("null")
        setEditingText("")                // applies changes made and resets editing input after changes been made

    }

    return (
        <div className="todo-container">
            <div className='checkbox-wrapper'>
                <input type="checkbox" checked={todo.complete} onChange={todoChecked} className="todo-checkbox" />
            </div>
            <div className='todo-text-wrapper'>
                <p className={todo.complete ? "checked" : ""}>{todo.name}</p>
                {/* <EdiText
                    type='text'
                    value={todo.name}
                    onSave={handleSave} /> */}
            </div>
            <div className='delete-button'><button onClick={deleteHandler} className="trash-btn"><i className="fas fa-trash"></i></button></div>
            <div>

                {todoEditing === todo.id ?           // Turnery operator that takes two values: Also checks if chosen todo to edit  =  id of clicked todo
                    (<input
                        type="text"
                        onChange={(e) => setEditingText(e.target.value)}     // if boolean value is true: show input field with todo's text
                        value={editingText}
                    />) :                                           // if boolean value is not true: show todo as text
                    ("")}


                {todoEditing === todo.id ? (<button className='Apply-btn' onClick={() => editTodo(todo.id)}> <i class="fas fa-check-square"></i> </button>)  // If todo is editing show submit button:  else show edit button
                    : (<button className='Edit-btn' onClick={() => setTodoEditing(todo.id)}> <i class="far fa-edit"></i> </button>)}


            </div>
        </div>


    )
}

export default Todo;
