import React from 'react'
import Todo from './Todo'

function TodoList({todos, toggleTodo, setTodos}) {
    return (
        todos.map((todo) => {
            return <Todo key={todo.id} todo={todo} toggleTodo={toggleTodo} todos={todos} setTodos={setTodos}/>
        })
    )
}

export default TodoList
