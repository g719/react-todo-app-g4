import React, { useState, useRef, useEffect } from 'react'
import './App.css';
import TodoList from './Components/TodoList';


function App() {

  const LOCAL_STORAGE_KEY = 'todoStorage'

  const [todos, setTodos] = useState([]);

  const inputText = useRef();

  const toggleTodo = (id, todo) => {
    const newTodos = [...todos] // copy of the todo array
    newTodos.find(todo => todo.id === id) // Finding todo with the right id that we click
    todo.complete = !todo.complete
    const sortedTodos = [...newTodos].sort((a, b) => a.complete - b.complete)
    // const sortedTodos = [...newTodos].sort(todo => todo.complete ? 1 : -1) // Sorting checked todos
    // 1. < 0 ... a comes first
    // 2. 0 ... nothing will be changed
    // 3. > 0 ... b comes first
    setTodos(sortedTodos);
  }
/*
  const [show, setShow] = useState(true)
 return(
   <div className='App'>
     {show?<h1>Hi</h1> :null}
     {}
   </div>
 )
*/

  const handleClearTodo = () => {
    const completedTodos = todos.filter(todo => !todo.complete)
    setTodos(completedTodos)
  }

  const handleAddTodo = () => {
    const input = inputText.current.value;
    if (input === '') return;
    setTodos(todos => {
      return [...todos, { name: input, id: Date.now(), complete: false }].sort((a, b) => a.complete - b.complete)
    })
    inputText.current.value = null; // resets the input when we add a todo.

  }

  // Get item from stored storage"todos"
  useEffect(() => {
    const storedTodos = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY))
    if (storedTodos) setTodos(storedTodos)
  }, []) // Runs only on the first render

  // Saves todos in local storage
  useEffect(() => {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todos))
  }, [todos])


  /*const Toggle = () =>{
    const togglebtn = document.querySelector(".toggle-btn");
    const divlist = document.querySelector(".todo-items-container");

    togglebtn.addEventListener("click", () =>{
      if (divlist.style.display === "none") {
        divlist.style.display = "block"
        togglebtn.innerHTML = "Hide"
      } else {
        divlist.style.display = "none"
        togglebtn.innerHTML = "Show "
      }
    })
  }
*/
  return (
    <>
      <div className='main-content-container'>
      
      <h1>Todo <span className='second'>A</span>pp</h1>
      <div className='todo-items-container'>
        <TodoList todos={todos} toggleTodo={toggleTodo} setTodos={setTodos} />
      </div>
      <div className="input-container">
        <input ref={inputText} type="text" placeholder='Write your todo here...' />
        <button className='add-btn' onClick={handleAddTodo}>Add</button>
        <button className='clear-btn' onClick={handleClearTodo}>Clear</button>
        {/* <button className='toggle-btn' onClick={Toggle}>Hide</button>  */}
      </div>
      
    </div>
      
    </>
  )
};

export default App